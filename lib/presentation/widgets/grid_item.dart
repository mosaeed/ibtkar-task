import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ibtkar_task/constants/strings.dart';
import 'package:ibtkar_task/data/models/PersonImagesResponse.dart';


class GridItem extends StatelessWidget {
  final Profiles layout;
  GridItem({required this.layout});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(IMAGE_VIEW_ROUTE,arguments: json.encode(layout));
      },
      child: Card(
        color: Colors.white,
        child: Center(
          child:   (layout.filePath != null)
              ? Image.network(
            "$IMAGES_BASE_URL${layout.filePath}",fit: BoxFit.fitWidth,)
              : Container(
            child: const Icon(Icons.image_search),
          ),
        ),
      ),
    );
  }
}