import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class General {
  static sizeBoxHorizontal(double space) {
    return SizedBox(
      width: space,
    );
  }

  static sizeBoxVertical(double space) {
    return SizedBox(
      height: space,
    );
  }


  static showSnackBar({required message, required BuildContext context}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        textAlign: TextAlign.center,
        // style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
      ),
      duration: Duration(seconds: 1),
      // backgroundColor: Colors.,
    ));
  }

  static void checkInternetThenMake({required BuildContext context,required Function action}) async {
    bool isConnectingToInternet =
    await InternetConnectionChecker().hasConnection;

    if (isConnectingToInternet == true) {
      action();
    } else {
      General.showSnackBar(
          message: "There is no internet connection", context: context);
    }
  }

   static isConnectingToInternet({required BuildContext context}) async {
    bool isConnectingToInternet =
    await InternetConnectionChecker().hasConnection;

    return isConnectingToInternet;
  }

}
