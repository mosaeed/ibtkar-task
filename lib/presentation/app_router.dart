import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ibtkar_task/cubit/home_screen/home_screen_cubit.dart';
import 'package:ibtkar_task/cubit/person_details_screen/person_details_screen_cubit.dart';
import 'package:ibtkar_task/data/models/PersonImagesResponse.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:ibtkar_task/data/network_service.dart';
import 'package:ibtkar_task/data/repository.dart';
import 'package:ibtkar_task/presentation/screens/image_view_screen.dart';
import 'package:ibtkar_task/presentation/screens/person_details_screen.dart';
import 'package:ibtkar_task/presentation/screens/home_screen.dart';

import '../constants/strings.dart';

class AppRouter {
  Repository? repository;

  AppRouter() {
    repository = Repository(networkService: NetworkService());
  }

  Route? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HOME_ROUTE:
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (_) => HomeScreenCubit(repository: repository!),
                  child: HomeScreen(),
                ));

      case PERSON_DETAILS_ROUTE:
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (_) =>
                      PersonDetailsScreenCubit(repository: repository!),
                  child: PersonDetailsScreen(
                    result: Results.fromJson(
                        json.decode(settings.arguments as String)),
                  ),
                ));

      case IMAGE_VIEW_ROUTE:
        return MaterialPageRoute(
            builder: (_) => ImageViewScreen(
                  imageData: Profiles.fromJson(
                      json.decode(settings.arguments as String)),
                ));
    }
  }
}
