import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ibtkar_task/constants/strings.dart';
import 'package:ibtkar_task/cubit/home_screen/home_screen_cubit.dart';
import 'package:ibtkar_task/cubit/person_details_screen/person_details_screen_cubit.dart';
import 'package:ibtkar_task/data/models/PersonDetailsResponse.dart';
import 'package:ibtkar_task/data/models/PersonImagesResponse.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:ibtkar_task/presentation/widgets/grid_item.dart';
import 'package:ibtkar_task/presentation/widgets/general.dart';

class PersonDetailsScreen extends StatefulWidget {
  final Results? result;

  const PersonDetailsScreen({Key? key, this.result}) : super(key: key);

  @override
  State<PersonDetailsScreen> createState() =>
      _PersonDetailsScreenState(result: result);
}

class _PersonDetailsScreenState extends State<PersonDetailsScreen> {
  final Results? result;
  PersonDetailsResponse? personDetailsResponse;
  PersonImagesResponse? personImagesResponse;
  int savedItemPosition = 0;

  _PersonDetailsScreenState({this.result});

  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();
    BlocProvider.of<PersonDetailsScreenCubit>(context)
        .getPersonDetails(context: context, personId: result!.id!);

    BlocProvider.of<PersonDetailsScreenCubit>(context)
        .getPersonImages(context: context, personId: result!.id!);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding: const EdgeInsets.only(left: 16,top: 30),
                        alignment: Alignment.center,
                        child: const Icon(Icons.arrow_back),
                      )),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(top: 30),
                      width: double.infinity,
                      child: Text(
                        result!.name!,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff808080)),
                      ),
                    ),
                  ),
                ],
              ),
              General.sizeBoxVertical(8),
              buildPersonDetailedView(),
              General.sizeBoxVertical(4),
            ],
          ),
        ),
      ),
    );
  }

  buildPersonDetailedView() {
    return Expanded(
        flex: 2,
        child: BlocConsumer<PersonDetailsScreenCubit, PersonDetailsScreenState>(
          listener: (context, state) {
            if (state is PersonDetailsScreenLoaded) {
              if (state.response != null) {
                personDetailsResponse = state.response!;
              }
            } else if (state is PersonImagesScreenLoaded) {
              if (state.response != null) {
                personImagesResponse = state.response!;
              }
            }
          },
          builder: (context, state) {
            if (personDetailsResponse != null) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    makeGridViewLayout(),
                    General.sizeBoxVertical(4),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Text(
                        "Popularity: ${personDetailsResponse!.popularity}",
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    General.sizeBoxVertical(4),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Text(getKnownAsText()),
                    ),
                    General.sizeBoxVertical(8),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Text(
                          "Birth: ${personDetailsResponse!.birthday}, at ${personDetailsResponse!.placeOfBirth}"),
                    ),
                    General.sizeBoxVertical(8),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Text("${personDetailsResponse!.biography}"),
                    ),
                    General.sizeBoxVertical(8),
                  ],
                ),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.grey,
                ),
              );
            }
          },
        ));
  }

  String getKnownAsText() {
    String knownAs = "Also Known as: ";
    for (String name in personDetailsResponse!.alsoKnownAs!) {
      knownAs += name + ", ";
    }

    return knownAs;
  }

  makeGridViewLayout() {
    if (personImagesResponse != null &&
        personImagesResponse!.profiles != null &&
        personImagesResponse!.profiles!.isNotEmpty) {
      return Container(
        height: 500,
        child: GridView.count(
          // physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          crossAxisCount: 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
          padding: EdgeInsets.all(5.0),
          children: List.generate(
            personImagesResponse!.profiles!.length,
            (index) => GridItem(
              layout: personImagesResponse!.profiles![index],
            ),
          ),
        ),
      );
    } else {
      return Container(
        height: 300,
        child: (result!.profilePath != null)
            ? Image.network(
                "$IMAGES_BASE_URL${result!.profilePath}",
                fit: BoxFit.fitWidth,
              )
            : Container(
                child: const Icon(Icons.image_search),
              ),
      );
    }
  }
}
