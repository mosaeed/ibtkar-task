import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ibtkar_task/constants/strings.dart';
import 'package:ibtkar_task/data/models/PersonDetailsResponse.dart';
import 'package:ibtkar_task/data/models/PersonImagesResponse.dart';
import 'package:ibtkar_task/presentation/widgets/general.dart';
import 'package:image_downloader/image_downloader.dart';

class ImageViewScreen extends StatefulWidget {
  final Profiles? imageData;


  const ImageViewScreen({Key? key, this.imageData}) : super(key: key);

  @override
  State<ImageViewScreen> createState() =>
      _ImageViewScreenState(imageData: imageData);
}

class _ImageViewScreenState extends State<ImageViewScreen> {
  final Profiles? imageData;
  PersonDetailsResponse? personDetailsResponse;
  PersonImagesResponse? personImagesResponse;
  int savedItemPosition = 0;

  String _message = "";
  String _path = "";
  String _size = "";
  String _mimeType = "";
  File? _imageFile;
  int _progress = 0;

  _ImageViewScreenState({this.imageData});

  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding: const EdgeInsets.only(left: 16,top: 30),
                        alignment: Alignment.center,
                        child: const Icon(Icons.arrow_back),
                      )),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(top: 30),
                      width: double.infinity,
                      child: const Text(
                        "Download Image",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff808080)),
                      ),
                    ),
                  ),
                ],
              ),
              General.sizeBoxVertical(8),
              AspectRatio(
                aspectRatio: imageData!.aspectRatio!,
                child: Container(
                  width: imageData!.width!.toDouble(),
                  height: imageData!.height!.toDouble(),
                  child: (imageData != null)
                      ? Image.network(
                          "$IMAGES_BASE_URL${imageData!.filePath!}",
                          fit: BoxFit.fitWidth,
                        )
                      : Container(
                          child: const Icon(Icons.image_search),
                        ),
                ),
              ),
              General.sizeBoxVertical(20),
              downloadBtn(),
              General.sizeBoxVertical(20),
            ],
          ),
        ),
      ),
    );
  }

  Widget downloadBtn() {
    return Container(
        height: 55,
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: ElevatedButton(
          onPressed: () => makeDownloadImage(),
          style: ButtonStyle(
            backgroundColor:
            MaterialStateProperty.all( Colors.blueGrey),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
                side: const BorderSide(color: Colors.blueGrey),
              ),
            ),
          ),
          child: const Text(
            "Download",
            style: TextStyle( fontSize: 14, color: Colors.white),
            textAlign: TextAlign.center,
          ),
        ));
  }

  makeDownloadImage() {
    if  (imageData != null) {
      _downloadImage(
          "$IMAGES_BASE_URL${imageData!.filePath!}");
    }

  }


  Future<void> _downloadImage(
      String url, {
        AndroidDestinationType? destination,
        bool whenError = false,
        String? outputMimeType,
      }) async {
    String? fileName;
    String? path;
    int? size;
    String? mimeType;
    try {
      String? imageId;

      if (whenError) {
        imageId = await ImageDownloader.downloadImage(url,
            outputMimeType: outputMimeType)
            .catchError((error) {
          if (error is PlatformException) {
            String? path = "";
            if (error.code == "404") {
              print("Not Found Error.");
            } else if (error.code == "unsupported_file") {
              print("UnSupported FIle Error.");
              path = error.details["unsupported_file_path"];
            }
            setState(() {
              _message = error.toString();
              _path = path ?? '';
            });
          }

          print(error);
        }).timeout(Duration(seconds: 10), onTimeout: () {
          print("timeout");
          return;
        });
      } else {
        if (destination == null) {
          imageId = await ImageDownloader.downloadImage(
            url,
            outputMimeType: outputMimeType,
          );
        } else {
          imageId = await ImageDownloader.downloadImage(
            url,
            destination: destination,
            outputMimeType: outputMimeType,
          );
        }
      }

      if (imageId == null) {
        return;
      }
      fileName = await ImageDownloader.findName(imageId);
      path = await ImageDownloader.findPath(imageId);
      size = await ImageDownloader.findByteSize(imageId);
      mimeType = await ImageDownloader.findMimeType(imageId);
    } on PlatformException catch (error) {
      setState(() {
        _message = error.message ?? '';
      });
      return;
    }

    if (!mounted) return;

    setState(() {
      var location = Platform.isAndroid ? "Directory" : "Photo Library";
      _message = 'Saved as "$fileName" in $location.\n';
      _size = 'size:     $size';
      _mimeType = 'mimeType: $mimeType';
      _path = path ?? '';

      if (!_mimeType.contains("video")) {
        _imageFile = File(path!);
      }
      return;
    });
  }
}
