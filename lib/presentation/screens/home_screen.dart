import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ibtkar_task/constants/strings.dart';
import 'package:ibtkar_task/cubit/home_screen/home_screen_cubit.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:ibtkar_task/data/sqllite.dart';
import 'package:ibtkar_task/presentation/widgets/general.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController _controller = new ScrollController();
  List<Results>? results = [];
  PopularPeopleResponse? popularPeopleResponse;
  int pageNumber = 1;
  int savedItemPosition = 0;

  @override
  void initState() {
    makeGetData();
    super.initState();
  }

  void makeGetData() async {
    bool internetStatus =
        await General.isConnectingToInternet(context: context);
    if (internetStatus) {
      WidgetsFlutterBinding.ensureInitialized();
      BlocProvider.of<HomeScreenCubit>(context)
          .getPopularPeople(context: context, pageNumber: pageNumber);
    } else {
      SqlDB.db.getSavedPersons().then((people) {
        setState(() {
          if (people != null && people.isNotEmpty) {
            print(people.first.toJson());
            results = people;
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 30),
                width: double.infinity,
                child: const Text(
                  "Popular People",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff808080)),
                ),
              ),
              General.sizeBoxVertical(24),
              buildPopularPeopleView(),
              General.sizeBoxVertical(24),
            ],
          ),
        ),
      ),
    );
  }

  buildPopularPeopleView() {
    return Expanded(
      flex: 2,
      child: BlocConsumer<HomeScreenCubit, HomeScreenState>(
        listener: (context, state) async {
          if (state is HomeScreenLoaded) {
            if (state.response != null &&
                state.response!.results != null &&
                state.response!.results!.isNotEmpty) {
              popularPeopleResponse = state.response!;
              results!.addAll(state.response!.results!);
              for (Results person in state.response!.results!) {
                await SqlDB.db.addNewPerson(person);
              }

              /*SqlDB.db.getUser().then((localUser) {
                _phoneNumber.controller.text = localUser['mobile'];
              });*/

            }
          }
        },
        builder: (context, state) {
          if (results != null && results!.isNotEmpty) {
            return NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
                  loadMore();
                }

                return true;
              },
              child: ListView.builder(
                itemCount: results!.length,
                // physics: const NeverScrollableScrollPhysics(),
                controller: _controller,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () async {
                      bool internetStatus =
                          await General.isConnectingToInternet(
                              context: context);
                      if (internetStatus) {
                        Navigator.of(context).pushNamed(PERSON_DETAILS_ROUTE,
                            arguments: json.encode(results![index]));
                      } else {
                        General.showSnackBar(
                            message:
                                "There is no internet connection to proceed",
                            context: context);
                      }
                    },
                    child: Card(
                        elevation: 4,
                        margin: const EdgeInsets.symmetric(
                          horizontal: 24,
                          vertical: 12,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          height: 100,
                          margin: const EdgeInsets.symmetric(
                            horizontal: 4,
                            vertical: 15,
                          ),
                          child: Row(
                            children: [
                              Container(
                                width: 100,
                                child: (results![index] != null &&
                                        results![index].profilePath != null)
                                    ? Image.network(
                                        "$IMAGES_BASE_URL${results![index].profilePath}")
                                    : Container(
                                        child: const Icon(Icons.image_search),
                                      ),
                              ),
                              General.sizeBoxHorizontal(20),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    (results![index].name != null)
                                        ? results![index].name!
                                        : "",
                                    style: const TextStyle(fontSize: 14),
                                  ),
                                  General.sizeBoxVertical(8),
                                  Text(
                                    (results![index].popularity != null)
                                        ? "Popularity: ${results![index].popularity!}"
                                        : "",
                                    style: const TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                  Text(
                                    (results![index].knownForDepartment != null)
                                        ? "Department: ${results![index].knownForDepartment!}"
                                        : "",
                                    style: const TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                ],
                              ),
                              const Expanded(
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.grey,
                                ),
                              )
                            ],
                          ),
                        )),
                  );
                },
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.grey,
              ),
            );
          }
        },
      ),
    );
  }

  Future<void> loadMore() async {
    bool internetStatus =
        await General.isConnectingToInternet(context: context);
    if (internetStatus) {}
    if (pageNumber != popularPeopleResponse!.totalPages!) {
      pageNumber++;

      BlocProvider.of<HomeScreenCubit>(context)
          .getPopularPeople(context: context, pageNumber: pageNumber);
    }
  }
}
