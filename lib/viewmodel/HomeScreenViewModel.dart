//
// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
// import 'package:ibtkar_task/data/network_service.dart';
// import 'package:ibtkar_task/data/repository.dart';
//
// // final homeScreenViewModel =
// // ChangeNotifierProvider.autoDispose((ref,int) => HomeScreenViewModel(pageNumber));
//
// final homeScreenViewModel = ChangeNotifierProvider.autoDispose.family<HomeScreenViewModel,Map<String,dynamic>>((ref,args) => HomeScreenViewModel(args: args));
//
//
// class HomeScreenViewModel extends ChangeNotifier {
//   Repository repository = Repository(networkService: NetworkService());
//
//   bool _loading = false;
//   int _totalPages = 0;
//   List<Results>? _results;
//
//   HomeScreenViewModel({
//     required Map<String,dynamic> args,}) {
//     getPopularPeople(pageNumber: args["pageNumber"], context:args["context"]);
//   }
//
//
//   bool get loading => _loading;
//
//   int get totalPages => _totalPages;
//
//   List<Results>? get results => _results;
//
//
//
//   getPopularPeople({
//     required int pageNumber,
//     required BuildContext context,
//   }) async {
//     setLoading(true);
//     var response = await repository.getPopularPeople(
//         pageNumber: pageNumber, context: context);
//     print(response);
//     if (response != null) {
//       setLoading(false);
//       setTotalPages(response.totalPages!);
//       setResultsList(
//         results: response.results!,
//       );
//     } else {
//       setLoading(false);
//     }
//   }
//
//   void setLoading(bool loading) {
//     _loading = loading;
//     notifyListeners();
//   }
//
//   void setTotalPages(int pages) {
//     _totalPages = pages;
//     notifyListeners();
//   }
//
//   // PopularPeopleResponse
//   void setResultsList({required List<Results> results}) {
//     _results!.addAll(results);
//     notifyListeners();
//   }
// }
