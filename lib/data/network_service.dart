import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:ibtkar_task/constants/strings.dart';
import 'package:ibtkar_task/data/models/PersonDetailsResponse.dart';
import 'package:ibtkar_task/data/models/PersonImagesResponse.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:ibtkar_task/presentation/widgets/general.dart';


class NetworkService {



  Future<PopularPeopleResponse?> getPopularPeople(
      {
        required int pageNumber,
        required BuildContext context,
      }) async {
    try {

      Map<String,dynamic> params = {
        "api_key": API_KEY,
        "language": LANGUAGE_TYPE,
        "page": "$pageNumber",
      };

      String? responseBody = await getServices(
          url: '3/person/popular',queryParams: params, context: context);

      if (responseBody != null) {
        Map<String, dynamic> responseJson = json.decode(responseBody);
        print('responseJson  :$responseJson');
        PopularPeopleResponse popularPeopleResponse = PopularPeopleResponse.fromJson(responseJson);

        log('popularPeopleResponse response  :${popularPeopleResponse.toJson()}');

        return popularPeopleResponse;
      } else {
        return null;
      }
    } catch (e) {
      throw e;
    }
  }

  Future<PersonDetailsResponse?> getPersonDetails(
      {
        required int personId,
        required BuildContext context,
      }) async {
    try {

      Map<String,dynamic> params = {
        "api_key": API_KEY,
        "language": LANGUAGE_TYPE,
      };

      String? responseBody = await getServices(
          url: '3/person/$personId',queryParams: params, context: context);

      if (responseBody != null) {
        Map<String, dynamic> responseJson = json.decode(responseBody);
        print('responseJson  :$responseJson');
        PersonDetailsResponse personDetailsResponse = PersonDetailsResponse.fromJson(responseJson);

        log('personDetailsResponse response  :${personDetailsResponse.toJson()}');

        return personDetailsResponse;
      } else {
        return null;
      }
    } catch (e) {
      throw e;
    }
  }

  Future<PersonImagesResponse?> getPersonImages(
      {
        required int personId,
        required BuildContext context,
      }) async {
    try {

      Map<String,dynamic> params = {
        "api_key": API_KEY,
        "language": LANGUAGE_TYPE,
      };

      String? responseBody = await getServices(
          url: '3/person/$personId/images',queryParams: params, context: context);

      if (responseBody != null) {
        Map<String, dynamic> responseJson = json.decode(responseBody);

        print('responseJson  :$responseJson');

        PersonImagesResponse personImagesResponse = PersonImagesResponse.fromJson(responseJson);

        log('personImagesResponse response  :${personImagesResponse.toJson()}');

        return personImagesResponse;
      } else {
        return null;
      }
    } catch (e) {
      throw e;
    }
  }


  Future<String?> getServices({required String url, required BuildContext context,
    Map<String, dynamic>? queryParams}) async {
    print('get url  is :$BASE_URL$url');
    if (queryParams != null) print("queryParams: ${queryParams.entries}");
    try {

      final uri = Uri.https(AUTHORITY_URL, url, queryParams);


      var response = await http.get(uri, headers: {
        // 'Content-Type': 'application/json',
        // 'Accept': 'application/json',
      }).timeout(const Duration(seconds: 60));

      print("response : $response");
      print("response statusCode : ${response.statusCode}");
      if (response.statusCode >= SUCCESS_CODE_FROM &&
          response.statusCode < SUCCESS_CODE_TO) {
        return response.body;
      } else if (response.statusCode == SERVER_ERROR_CODE) {
        General.showSnackBar(message: "Server Error", context: context);
      }else {
        var responseJson = json.decode(response.body);
        print("responseError stack is:$responseJson");

        General.showSnackBar(message: "$responseJson", context: context);
        throw Exception(responseJson);
      }
    } catch (err) {
      print('catch get responseError is :$err');
      General.showSnackBar(message: "${err.toString()}", context: context);
      throw err;
    }
  }
}
