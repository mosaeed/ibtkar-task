import 'package:flutter/cupertino.dart';
import 'package:ibtkar_task/data/models/PersonDetailsResponse.dart';
import 'package:ibtkar_task/data/models/PersonImagesResponse.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'network_service.dart';

class Repository {
  final NetworkService networkService;

  Repository({required this.networkService});

  Future<PopularPeopleResponse?> getPopularPeople({
    required int pageNumber,
    required BuildContext context,
  }) async {
    return networkService.getPopularPeople(
      pageNumber: pageNumber,
      context: context,
    );
  }

  Future<PersonDetailsResponse?> getPersonDetails({
    required int personId,
    required BuildContext context,
  }) async {
    return networkService.getPersonDetails(
      personId: personId,
      context: context,
    );
  }

  Future<PersonImagesResponse?> getPersonImages({
    required int personId,
    required BuildContext context,
  }) async {
    return networkService.getPersonImages(
      personId: personId,
      context: context,
    );
  }


}
