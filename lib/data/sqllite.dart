import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';


class SqlDB {

  static final SqlDB db = SqlDB();
  static Database? _db;
  String _tableName = 'person';

  Future<Database> get database async {
    if (_db != null) {
      return _db!;
    }
    _db = await initSqlDB();
    return _db!;
  }

  initSqlDB() async {
    return await openDatabase(
      join(await getDatabasesPath(), 'ibtkar_task_local_storge'),
      onCreate: (db, version) async {
        await db.execute('''
         CREATE TABLE $_tableName(
         name STRING, popularity DOUBLE, profilePath STRING, knownForDepartment STRING,personId INT
         )       
          ''');
      },
      version: 1,
    );
  }

  addNewPerson(Results newPerson) async {
    // deleteUser();
    final db =  await database;

    var response = db.rawInsert(''' 
    INSERT INTO $_tableName(
      name, popularity, profilePath, knownForDepartment, personId
    ) VALUES (?,?,?,?,?)
    ''',[newPerson.name,newPerson.popularity,newPerson.profilePath,newPerson.knownForDepartment,newPerson.id]);

    return response;
  }

  /*deleteUser()async{
    final db =  await database;
    db.rawDelete("Delete from $_tableName");
  }*/

  Future<List<Results>?> getSavedPersons() async {
    final db =  await database;
    var response = await db.query(_tableName);
    if (response.length == 0){
      return null;
    }else{
      var persons = response;
      List<Results>? results=[];
      for(Map<String, Object?> item in persons){
        results.add(Results.fromJson(item));
      }
      return results.isNotEmpty ? results: null ;
    }
  }

}