import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:ibtkar_task/data/repository.dart';

part 'home_screen_state.dart';

class HomeScreenCubit extends Cubit<HomeScreenState> {
  final Repository repository;

  HomeScreenCubit({required this.repository}) : super(HomeScreenInitial());

  void getPopularPeople(
      {
        required int pageNumber,
      required BuildContext context}) {
    emit(HomeScreenLoading());
    repository
        .getPopularPeople(
      pageNumber: pageNumber,
            context: context)
        .then((response) {
      emit(HomeScreenLoaded(response: response));
    });
  }
}
