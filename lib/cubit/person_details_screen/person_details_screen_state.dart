part of 'person_details_screen_cubit.dart';

@immutable
abstract class PersonDetailsScreenState {}

class PersonDetailsScreenInitial extends PersonDetailsScreenState {}
class PersonDetailsScreenLoading extends PersonDetailsScreenState {}

class PersonDetailsScreenLoaded extends PersonDetailsScreenState {
  final PersonDetailsResponse? response;
  PersonDetailsScreenLoaded({this.response});
}

class PersonImagesScreenLoaded extends PersonDetailsScreenState {
  final PersonImagesResponse? response;
  PersonImagesScreenLoaded({this.response});
}


