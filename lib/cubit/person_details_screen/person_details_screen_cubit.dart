import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:ibtkar_task/data/models/PersonDetailsResponse.dart';
import 'package:ibtkar_task/data/models/PopularPeopleResponse.dart';
import 'package:ibtkar_task/data/repository.dart';

import '../../data/models/PersonImagesResponse.dart';

part 'person_details_screen_state.dart';

class PersonDetailsScreenCubit extends Cubit<PersonDetailsScreenState> {
  final Repository repository;

  PersonDetailsScreenCubit({required this.repository})
      : super(PersonDetailsScreenInitial());

  void getPersonDetails(
      {required int personId, required BuildContext context}) {
    emit(PersonDetailsScreenLoading());
    repository
        .getPersonDetails(personId: personId, context: context)
        .then((response) {
      emit(PersonDetailsScreenLoaded(response: response));
    });
  }

  void getPersonImages(
      {required int personId, required BuildContext context}) {
    emit(PersonDetailsScreenLoading());
    repository
        .getPersonImages(personId: personId, context: context)
        .then((response) {
      emit(PersonImagesScreenLoaded(response: response));
    });
  }

}
