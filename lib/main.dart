import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ibtkar_task/presentation/app_router.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:sizer/sizer.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
      app(),
  );
}

Widget app() {
  return Sizer(
    builder: (context, orientation, deviceType) =>
        MyApp(router: AppRouter()),
  );
}

class MyApp extends StatefulWidget {
  final AppRouter router;

  MyApp({Key? key, required this.router});

  @override
  _MyAppState createState() => _MyAppState(router);
}

class _MyAppState extends State<MyApp> {
  final AppRouter router;

  ThemeData appTheme = ThemeData(
    primarySwatch: Colors.blue,
    fontFamily: "Tajawal-Regular",
    primaryColor: const Color(0xff7F45FF),
    backgroundColor: const Color(0xffB1B6C0),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: Color(0xff000000),
      selectionColor: Color(0xffF5F5F5),
      selectionHandleColor: Color(0xffF5F5F5),
    ),
    brightness: Brightness.light,
    scaffoldBackgroundColor: Colors.white,
  );

  _MyAppState(this.router);

  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return OverlaySupport.global(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Ibtkar Task',
        theme: appTheme,
        onGenerateRoute: router.generateRoute,
      ),
    );
  }
}