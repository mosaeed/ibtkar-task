

 const String BASE_URL = "https://api.themoviedb.org/";
 const String AUTHORITY_URL = "api.themoviedb.org";
 const String IMAGES_BASE_URL = "https://image.tmdb.org/t/p/w500";
 const String API_KEY = "372150aa3d6d5e53a0d4b04a1acb5316";
 const String LANGUAGE_TYPE = "en-US";

 // Route
 const String HOME_ROUTE = '/';
 const String PERSON_DETAILS_ROUTE = '/PERSON_DETAILS';
 const String IMAGE_VIEW_ROUTE = '/Image_VIEW';


 const int SUCCESS_CODE_FROM = 200;
 const int SUCCESS_CODE_TO = 300;
 const int SUCCESS_CODE = 200;
 const int SUCCESS_CODE_SECOND = 201;
 const int SUCCESS_CODE_Third = 202;
 const int NOT_ACTIVATED_CODE = 417;
 const int LOGGED_IN_BEFORE_CODE = 401;
 const int NOT_FOUND_CODE = 404;
 const int SERVER_ERROR_CODE = 500;
 const int WAIT_CODE = 429;
 const int INVALED_DATA_CODE = 422;